from .base import *


ALLOWED_HOSTS = ['*']

BASE_DIR = Path(__file__).resolve().parent.parent

# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'db_promotions_qa',
        'USER': 'user_promotions',
        'PASSWORD': 'admin',
        'HOST': '10.130.2.175',
        'PORT': '3306',
    }
}

STATIC_URL = '/static/'
