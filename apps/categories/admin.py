from django.contrib import admin

from apps.categories.models import CategoryProduct,CategoryStore

admin.site.register(CategoryStore)
admin.site.register(CategoryProduct)