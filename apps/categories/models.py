from django.db import models
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel


class CategoryProduct(BaseModel):
    """Model definition for CategoryProduct."""

    name = models.CharField('Nombre de Categoría', max_length = 100,blank=False, null=False, unique=True)
    description = models.TextField('Descripción de Categoría', blank = True,null = False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for CategoryProduct."""
        
        db_table = 'T_PMT_CATEGORY_PRODUCT'
        verbose_name = 'Categoría de Producto'
        verbose_name_plural = 'Categorías de Productos'

    def __str__(self):
        """Unicode representation of CategoryProduct."""
        return self.name


class CategoryStore(BaseModel):
    """Model definition for CategoryStore."""

    name = models.CharField('Nombre de Categoría', max_length = 100,blank=False, null=False, unique=True)
    description = models.TextField('Descripción de Categoría', blank = True,null = False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for CategoryStore."""
        
        db_table = 'T_PMT_CATEGORY_STORE'
        verbose_name = 'Categoría de Tienda'
        verbose_name_plural = 'Categorías de Tienda'

    def __str__(self):
        """Unicode representation of CategoryTienda."""
        return self.name

