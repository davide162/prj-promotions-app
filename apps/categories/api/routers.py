from rest_framework.routers import DefaultRouter
from apps.categories.api.views import CategoryProductViewSet, CategoryStoreViewSet

router = DefaultRouter()

router.register(r'category-products',CategoryProductViewSet,basename = 'category_products')
router.register(r'category-store',CategoryStoreViewSet,basename = 'category_store')

urlpatterns = router.urls