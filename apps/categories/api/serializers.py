from rest_framework import serializers

from apps.categories.models import CategoryProduct,CategoryStore

class CategoryProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryProduct
        exclude = ('state','created_date','modified_date','deleted_date')


    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description
        }

class CategoryStoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryStore
        exclude = ('state','created_date','modified_date','deleted_date')

    def to_representation(self,instance):
        return {
            'id': instance.id,
            'name': instance.name,
            'description': instance.description
        }
