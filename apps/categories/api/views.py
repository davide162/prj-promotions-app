from rest_framework import generics
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from apps.base.api import GeneralListApiView
from apps.users.authentication_mixins import Authentication
from apps.categories.api.serializers import CategoryProductSerializer,CategoryStoreSerializer

class CategoryProductViewSet(Authentication,viewsets.ModelViewSet):
    serializer_class = CategoryProductSerializer

    def get_queryset(self,pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.filter(state = True)
        return self.get_serializer().Meta.model.objects.filter(id = pk,state = True).first()

    def list(self,request):
        category_serializer = self.get_serializer(self.get_queryset(),many = True)
        return Response(category_serializer.data,status = status.HTTP_200_OK)

    def create(self,request):
        # send information to serializer
        category_serializer = self.serializer_class(data = request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            return Response({'message': 'Categoría de Producto creada correctamente!'},status = status.HTTP_201_CREATED)
        return Response(category_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    def update(self,request,pk=None):
        if self.get_queryset(pk):
            # send information to serializer referencing the instance
            category_serializer = self.serializer_class(self.get_queryset(pk),data = request.data)            
            if category_serializer.is_valid():
                category_serializer.save()
                return Response(category_serializer.data,status = status.HTTP_200_OK)
            return Response(category_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    def destroy(self,request,pk=None):
        category = self.get_queryset().filter(id = pk).first() # get instance        
        if category:
            category.state = False
            category.save()
            return Response({'message':'Categoría de Producto eliminado correctamente!'},status = status.HTTP_200_OK)
        return Response({'error':'No existe una Categoría de Producto con estos datos!'},status = status.HTTP_400_BAD_REQUEST)


class CategoryStoreViewSet(Authentication,viewsets.ModelViewSet):
    serializer_class = CategoryStoreSerializer

    def get_queryset(self,pk=None):
        if pk is None:
            return self.get_serializer().Meta.model.objects.filter(state = True)
        return self.get_serializer().Meta.model.objects.filter(id = pk, state = True).first()

    def list(self,request):
        category_serializer = self.get_serializer(self.get_queryset(),many = True)
        return Response(category_serializer.data,status = status.HTTP_200_OK)

    def create(self,request):
        # send information to serializer
        category_serializer = self.serializer_class(data = request.data)
        if category_serializer.is_valid():
            category_serializer.save()
            return Response({'message': 'Categoría de Tienda creada correctamente!'},status = status.HTTP_201_CREATED)
        return Response(category_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    def update(self,request,pk=None):
        if self.get_queryset(pk):
            # send information to serializer referencing the instance
            category_serializer = self.serializer_class(self.get_queryset(pk),data = request.data)            
            if category_serializer.is_valid():
                category_serializer.save()
                return Response(category_serializer.data,status = status.HTTP_200_OK)
            return Response(category_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    def destroy(self,request,pk=None):
        category = self.get_queryset().filter(id = pk).first() # get instance        
        if category:
            category.state = False
            category.save()
            return Response({'message':'Categoría de Tienda eliminado correctamente!'},status = status.HTTP_200_OK)
        return Response({'error':'No existe una Categoría de Tienda con estos datos!'},status = status.HTTP_400_BAD_REQUEST)
