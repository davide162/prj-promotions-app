from django.db import models
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel
from apps.products.models import Product

# Create your models here.
class Coupon(BaseModel):
    """Model definition for Coupns."""

    product = models.ForeignKey(Product, on_delete=models.CASCADE,verbose_name = 'Producto', null = True)
    discount_percent = models.FloatField('Descuento', blank=False, null=False)
    date_begin = models.DateTimeField('Fecha inicio', blank=False, null=False)
    date_end = models.DateTimeField('Fecha Fin', blank=False, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Product."""

        db_table = 'T_PMT_COUPONS'
        verbose_name = 'Cupón'
        verbose_name_plural = 'Cupones'

    def __str__(self):
        """Unicode representation of Coupons."""
        return f'Tienda: {self.product.store.name} -- Producto: {self.product.name}%' 
        