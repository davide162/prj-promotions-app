from django.contrib import admin

from apps.coupons.models import Coupon
# Register your models here.
admin.site.register(Coupon)
