from django.db import models
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel
from apps.stores.models import Store
from apps.categories.models import CategoryProduct

# Create your models here.
class Product(BaseModel):
    """Model definition for Product."""

    store = models.ForeignKey(Store, on_delete=models.CASCADE,verbose_name = 'Tienda', null = True)
    category_product = models.ForeignKey(CategoryProduct, on_delete=models.CASCADE,verbose_name = 'Categoría de Producto', null = True)
    name = models.CharField('Nombre de Producto', max_length=150, unique = True,blank = False,null = False)
    description = models.TextField('Descripción de Producto',blank = True,null = False)
    image = models.ImageField('Imagen del Producto', upload_to='products/',blank = True,null = True)
    price = models.FloatField('Precio', blank=False, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Product."""

        db_table = 'T_PMT_PRODUCTS'
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'

    def __str__(self):
        """Unicode representation of Product."""
        return self.name