from datetime import datetime

from django.contrib.sessions.models import Session

from rest_framework import status
from rest_framework import renderers
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import ugettext as _
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken

from apps.users.models import User,Login
from apps.users.api.serializers import LoginTokenSerializer,LoginGetTokenSerializer,UserSerializer

class CustomObtainAuthToken(ObtainAuthToken):
    
    serializer_class = LoginGetTokenSerializer
    renderer_classes = (renderers.JSONRenderer,)

class UserRefreshToken(APIView):
    """
    Return Token for an username sended
    """
    def get(self,request,*args,**kwargs):
        email = request.GET.get('email')
        if email is not None:
            try:
                #login = Login.objects.filter(email=email,is_active=True).first()
                login= LoginTokenSerializer().Meta.model.objects.filter(email=email,is_active=True).first()
                user_token = Token.objects.get(
                    user = login
                )
                return Response({
                    'token': user_token.key
                })
            except:
                return Response({
                    'error': _('Credenciales enviadas incorrectas')
                },status = status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                    'error': _('Datos insuficientes, se debe especificar el ultimo token')
                },status = status.HTTP_400_BAD_REQUEST)

class UserGetToken(CustomObtainAuthToken):
    """Servicio que autentica un usuario en la web promotions y retorna un token de autenticación

    Args:
        json :usuario,contraseña
    """
    def post(self,request,*args,**kwargs):
        # send to serializer username and password
        login_serializer = self.serializer_class(data = request.data, context = {'request':request})
        if login_serializer.is_valid():
            # login serializer return user in validated_data
            login = login_serializer.validated_data['user']
            user = User.objects.get(login_id = login.id)
            if login.is_active:
                token,created = Token.objects.get_or_create(user = login)
                user_serializer = UserSerializer(user)
                if created:
                    return Response({
                        'token': token.key,
                        'user': user_serializer.data,
                        'message': 'Inicio de Sesión Exitoso.'
                    }, status = status.HTTP_201_CREATED)
                else:
                    token.delete()
                    token = Token.objects.create(user = login)
                    return Response({
                        'token': token.key,
                        'user': user_serializer.data,
                        'message': 'Inicio de Sesión Exitoso.'
                    }, status = status.HTTP_201_CREATED)
            else:
                return Response({'error':_('Este usuario no puede iniciar sesión')}, 
                                    status = status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({'error': _('Nombre de usuario o contraseña incorrectos')},
                                    status = status.HTTP_400_BAD_REQUEST)

class UserLogout(APIView):

    def get(self,request,*args,**kwargs):
        try:
            token = request.GET.get('token')
            token = Token.objects.filter(key = token).first()

            if token:
                user = token.user
                # delete all sessions for user
                all_sessions = Session.objects.filter(expire_date__gte = datetime.now())
                if all_sessions.exists():
                    for session in all_sessions:
                        session_data = session.get_decoded()
                        # search auth_user_id, this field is primary_key's user on the session
                        if user.id == int(session_data.get('_auth_user_id')):
                            session.delete()
                # delete user token
                token.delete()
                
                session_message = 'Sesiones de usuario eliminadas.'  
                token_message = 'Token eliminado.'
                return Response({'token_message': token_message,'session_message':session_message},
                                    status = status.HTTP_200_OK)
            
            return Response({'error':'No se ha encontrado un usuario con estas credenciales.'},
                    status = status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'error': _('No se ha encontrado token en la petición')}, 
                                    status = status.HTTP_409_CONFLICT)

