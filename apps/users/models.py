from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin

class LoginManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        user = self.model(
            email = email,
            is_staff = is_staff,
            is_superuser = is_superuser,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)

class Login(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('Correo Electrónico',max_length = 255, unique = True,)
    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    historical = HistoricalRecords()
    objects = LoginManager()

    class Meta:
        db_table = 'T_PMT_LOGIN'
        verbose_name = 'Login'
        verbose_name_plural = 'Logins'

    USERNAME_FIELD = 'email'
    #REQUIRED_FIELDS = ['name','last_name']

    def __str__(self):
        return f'{self.email}'

class TypeProfile(models.TextChoices):
    ADMIN_TIENDA = 'ADMINISTRADOR_TIENDA', 'Administrador Tienda'
    EMPLEADO_TIENDA = 'TIENDA', 'Empleado Tienda'
    CLIENTE_APP = 'CLIENTE_APP', 'Cliente App'
    ADMINISTRADOR = 'ADMINISTRADOR', 'Administrador'

class User(BaseModel):
    login = models.OneToOneField(Login,related_name='login_id', on_delete=models.RESTRICT)
    type_profile = models.CharField(max_length=25,choices=TypeProfile.choices,default=TypeProfile.CLIENTE_APP,)
    name = models.CharField('Nombre', max_length = 255, blank = True, null = True)
    last_name = models.CharField('Apellidos', max_length = 255, blank = True, null = True)
    image = models.ImageField('Imagen de perfil', upload_to='perfil/', max_length=255, null=True, blank = True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    historical = HistoricalRecords()


    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        db_table = 'T_PMT_USER'
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return f'{self.name} {self.last_name} --{self.login.email}-- ({self.type_profile})'
