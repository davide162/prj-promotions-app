# Generated by Django 3.2 on 2021-05-01 03:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20210430_2214'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicaluser',
            name='created_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Creación'),
        ),
        migrations.AddField(
            model_name='historicaluser',
            name='deleted_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Eliminación'),
        ),
        migrations.AddField(
            model_name='historicaluser',
            name='modified_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Modificación'),
        ),
        migrations.AddField(
            model_name='historicaluser',
            name='state',
            field=models.BooleanField(default=True, verbose_name='Estado'),
        ),
        migrations.AddField(
            model_name='user',
            name='created_date',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='Fecha de Creación'),
        ),
        migrations.AddField(
            model_name='user',
            name='deleted_date',
            field=models.DateField(auto_now=True, null=True, verbose_name='Fecha de Eliminación'),
        ),
        migrations.AddField(
            model_name='user',
            name='modified_date',
            field=models.DateField(auto_now=True, null=True, verbose_name='Fecha de Modificación'),
        ),
        migrations.AddField(
            model_name='user',
            name='state',
            field=models.BooleanField(default=True, verbose_name='Estado'),
        ),
        migrations.AlterField(
            model_name='historicaluser',
            name='id',
            field=models.IntegerField(blank=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
