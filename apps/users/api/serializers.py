
from rest_framework import serializers
from django.contrib.auth import authenticate

from apps.users.models import Login,User

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Login
        exclude = ['id','password','groups','user_permissions','last_login','is_staff','is_superuser']

class LoginTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Login
        fields = ('email','password')

class LoginGetTokenSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField(style={'input_type': 'name'})

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')

        if email and password:
            user = authenticate(email=email, password=password)

            if user is not None:
                data['user'] = user
                return data
            else:
                message = ('Usuario o contraseña incorrectos.')
                raise serializers.ValidationError(message)

        message = ('Los campos usuario y contraseña son requeridos.')
        raise serializers.ValidationError(message)
        return data

class LoginRegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Login
        fields = '__all__'

    def create(self,validated_data):
        login = Login(**validated_data)
        login.set_password(validated_data['password'])
        login.save()
        return login

    def update(self,instance,validated_data):
        updated_user = super().update(instance,validated_data)
        
        if 'password' in validated_data:
            updated_user.set_password(validated_data['password'])
    
        updated_user.save()
        return updated_user

class LoginListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Login

    def to_representation(self,instance):
        return {
            'email': instance['email'],
            'is_superuser': instance['is_superuser'],
            'is_active': instance['is_active'],
            'last_login': instance['last_login'],
        }

class UserSerializer(serializers.ModelSerializer):
    login = LoginSerializer(required=True)
    
    class Meta:
        model = User
        fields = ['id','type_profile','name','last_name','image','location','birth_date','login']

class UserRegisterSerializer(serializers.ModelSerializer):
    login = LoginRegisterSerializer()

    class Meta:
        model = User
        exclude = ('state','created_date','modified_date','deleted_date')

    def create(self,validated_data):
        validated_login_data = validated_data.pop('login')
        login = Login(**validated_login_data)
        login.set_password(validated_login_data['password'])
        login.save()
        user=User(login =login,**validated_data)
        user.save()
        return user





