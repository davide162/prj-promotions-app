from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.renderers import JSONRenderer
from django.utils.translation import ugettext as _

from apps.users.authentication_mixins import Authentication
from apps.users.models import Login,User
from apps.users.api.serializers import *

@api_view(['GET','POST'])
def login_api_view(request):
    token = Authentication().get_user(request)

    if(token != None):
        # list
        if request.method == 'GET':
            # queryset
            logins = Login.objects.filter(is_active=True).values('email','is_superuser','is_active','last_login')
            logins_serializer = LoginListSerializer(logins,many = True)
            return Response(logins_serializer.data,status = status.HTTP_200_OK)
        # create
        elif request.method == 'POST':
            logins_serializer = LoginRegisterSerializer(data = request.data)

            # validation
            if logins_serializer.is_valid():
                logins_serializer.save()
                return Response({'message':_('Login creado correctamente')},status = status.HTTP_201_CREATED)

            return Response(logins_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    return Response({'message':_('No tiene privilegios')},status = status.HTTP_201_CREATED)

@api_view(['GET','PUT','PATCH','DELETE'])
def login_detail_api_view(request,pk=None):
    token = Authentication().get_user(request)

    if(token != None):
        # queryset
        login = Login.objects.filter(id = pk).first()

        # validation
        if login:

            # retrieve
            if request.method == 'GET': 
                login_serializer = LoginSerializer(login)
                return Response(login_serializer.data,status = status.HTTP_200_OK)
            
            # update partial
            elif request.method == 'PUT':
                login_serializer = LoginRegisterSerializer(login,data = request.data,partial=True)
                if login_serializer.is_valid():
                    login_serializer.save()
                    return Response(login_serializer.data,status = status.HTTP_200_OK)
                return Response(login_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

            # delete
            elif request.method == 'DELETE':
                login.delete()
                return Response({'message':_('Login Eliminado correctamente')},status = status.HTTP_200_OK)
        
        return Response({'message':_('No se ha encontrado un usuario con estos datos')},status = status.HTTP_400_BAD_REQUEST)
    
    return Response({'message':_('No tiene privilegios')},status = status.HTTP_201_CREATED)

@api_view(['GET','POST'])
def user_api_view(request):
    token = Authentication().get_user(request)

    if(token != None):
        if request.method == 'GET':
            # queryset
            users = User.objects.filter(state=True)
            users_serializer = UserSerializer(users,many = True)
            return Response(users_serializer.data,status = status.HTTP_200_OK)
        # create
        elif request.method == 'POST':
            users_serializer = UserRegisterSerializer(data = request.data)

            # validation
            if users_serializer.is_valid():
                users_serializer.save()
                return Response({'message':_('Usuario creado correctamente')},status = status.HTTP_201_CREATED)

            return Response(users_serializer.errors,status = status.HTTP_400_BAD_REQUEST)

    return Response({'message':_('No tiene privilegios')},status = status.HTTP_201_CREATED)

