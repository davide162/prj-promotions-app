  
from django.urls import path
from apps.users.api.api import login_api_view,login_detail_api_view,user_api_view

urlpatterns = [
    path('login/',login_api_view, name = 'login_api'),
    path('login/<int:pk>/',login_detail_api_view, name = 'login_detail_api_view'),
    path('usuario/',user_api_view, name = 'user_api'),
]