from django.db import models
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel
from apps.coupons.models import Coupon

# Create your models here.
class Inventory(BaseModel):
    """Model definition for Inventory."""

    products_coupons = models.ForeignKey(Coupon, on_delete=models.CASCADE,verbose_name = 'Cupones', null = True)
    number = models.IntegerField('Cantidad de cupones', blank=False, null=False)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Inventory."""

        db_table = 'T_PMT_INVENTORY'
        verbose_name = 'Inventario'
        verbose_name_plural = 'Inventarios'

    def __str__(self):
        """Unicode representation of Inventory."""
        return self.products_coupons.product.name