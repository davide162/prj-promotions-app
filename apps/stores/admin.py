from django.contrib import admin

from apps.stores.models import Store
# Register your models here.
admin.site.register(Store)
