# Generated by Django 3.2 on 2021-05-01 03:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stores', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalstore',
            name='created_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Creación'),
        ),
        migrations.AlterField(
            model_name='historicalstore',
            name='deleted_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Eliminación'),
        ),
        migrations.AlterField(
            model_name='historicalstore',
            name='modified_date',
            field=models.DateField(blank=True, editable=False, null=True, verbose_name='Fecha de Modificación'),
        ),
        migrations.AlterField(
            model_name='store',
            name='created_date',
            field=models.DateField(auto_now_add=True, null=True, verbose_name='Fecha de Creación'),
        ),
        migrations.AlterField(
            model_name='store',
            name='deleted_date',
            field=models.DateField(auto_now=True, null=True, verbose_name='Fecha de Eliminación'),
        ),
        migrations.AlterField(
            model_name='store',
            name='modified_date',
            field=models.DateField(auto_now=True, null=True, verbose_name='Fecha de Modificación'),
        ),
    ]
