from django.db import models
from simple_history.models import HistoricalRecords

from apps.base.models import BaseModel
from apps.users.models import User
from apps.categories.models import CategoryStore

# Create your models here.
class Store(BaseModel):
    """Model definition for Store."""
    user = models.ManyToManyField(User, related_name='user_id')
    category_store = models.OneToOneField(CategoryStore, related_name='category_id', on_delete=models.RESTRICT)
    name = models.CharField('Nombre de Tienda', max_length=150, unique = True,blank = False,null = False)
    description = models.TextField('Descripción de Tienda',blank = True,null = True)
    image = models.ImageField('Logo del Tienda', upload_to='products/',blank = True,null = True)
    historical = HistoricalRecords()

    @property
    def _history_user(self):
        return self.changed_by

    @_history_user.setter
    def _history_user(self, value):
        self.changed_by = value

    class Meta:
        """Meta definition for Store."""

        db_table = 'T_PMT_STORE'
        verbose_name = 'Tienda'
        verbose_name_plural = 'Tiendas'

    def __str__(self):
        """Unicode representation of Product."""
        return self.name

